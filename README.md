# yorgChart-frontEnd

"Your OrgChart" ==> YorgChart !
Organization charts that don't suck

# Data-driven org charts! ("'bout damn time!")
To play with just online:

1.  Go to the [hosted site](https://richardeschloss.gitlab.io/yorgchart-frontend/)

or

2. Check it out right here: (if I can get the damn iframes to work...)

<figure class="video_container">
    <iframe src="https://richardeschloss.gitlab.io/yorgchart-frontend/"></iframe>
</figure>

or

3. Check it out on your system:
> git clone [this]

> npm i

> npm run dev

Open up the browser to localhost:3000 and see the org chart. Change src/data/company.json and the page will live reload once you save it..



More details to follow, as well as the goal with this project.
